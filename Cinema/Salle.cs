﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class Salle
    {
        public int SalleId { get; set; }
        public int NbrPls { get; private set; }
        public string Name { get; private set; }
        public Movie Film = new Movie();
        DBConnect DB = new DBConnect();
        public override string ToString()
        {
            return (this.Name + " had " + this.NbrPls + " places");
        }
        public void CreateSalle()
        {
            Console.Write("Salle Name : ");
            string name = Console.ReadLine();
            Console.Write("Place Number : ");
            string input = Console.ReadLine();
            int nbr = 0;
            while (!int.TryParse(input, out nbr))
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: It's not a number retry");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Place Number : ");
                input = Console.ReadLine();
            }
            Salle salle = new Salle()
            {
                NbrPls = nbr,
                Name = name
            };
            DB.Salles.Add(salle);
            DB.SaveChanges();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Sucess New Salle Create");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(salle.ToString());
        }
        public int SetMovie()
        {
            Console.Write("Salle Name : ");
            string name = Console.ReadLine();
            var salleResult = (from s in DB.Salles where s.Name == name select s);
            if (!salleResult.Any())
            {
                return -1;
            }
            Salle salle = salleResult.First();
            Console.Write("Movie's Name : ");
            var MovieResult = (from m in DB.Movie where m.Name == name select m);
            if (!salleResult.Any())
            {
                return -2;
            }
            salle.Film = MovieResult.First();
            DB.SaveChanges();
            return 0;
        }

    }
}
