﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class Movie
    {
        public int MovieId { get; private set; }
        public string Name { get; private set; }
        public string Rea { get; private set; }
        public string Produc { get; private set; }
        public List<DateTime> Hour { get; private set; }
        public Salle Salle { get; private set; }
        DBConnect DB = new DBConnect();

        public override string ToString()
        {
            return this.Name + " had " + this.Rea + " for rea, " + this.Produc + " for productor, and in " + this.Salle.Name;
        }
        public int CreateMovie()
        {
            Console.Write("Movie Name : ");
            string name = Console.ReadLine();
            Console.Write("Rea Name : ");
            string rea = Console.ReadLine();
            Console.Write("Produc Name : ");
            string produc = Console.ReadLine();
            Console.Write("Salle: ");
            string sal = Console.ReadLine();
            var salleResult = (from s in DB.Salles where s.Name == sal select s);
            if (!salleResult.Any())
            {
                return -1;
            }
            Salle salle = salleResult.First();
            Movie movie = new Movie()
            {
                Name = name,
                Rea = rea,
                Produc = produc,
                Salle = salle,
            };
            DB.Movie.Add(movie);
            DB.SaveChanges();
            SetHour(name);
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Sucess New Movie Create");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(movie.ToString());
            return 0;

        }
        public int SetHour(string name)
        {
            var MovieResult = (from s in DB.Movie where s.Name == name select s);
            if (!MovieResult.Any())
            {
                return -1;
            }
            Movie movie = new Movie();
            Console.WriteLine("How many Hour you want add ? ");
            string input = Console.ReadLine();
            int nbr = 0;
            while (!int.TryParse(input, out nbr))
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: It's not a number retry");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("How many Hour you want add ? ");
                input = Console.ReadLine();
            }
            for (int i = 0; i < nbr; i++)
            {
                Console.WriteLine("Hour {0} ? ", i);
                input = Console.ReadLine();
                DateTime hour;
                List<DateTime> Hours = new List<DateTime>();
                while (!DateTime.TryParse(input, out hour))
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: It's not a date retry");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Hour {0} ? ", i);
                    input = Console.ReadLine();
                }
                Hours.Add(hour);
                movie.Hour = Hours;
                DB.SaveChanges();
            }
            return 0;
        }

        public void MovieList()
        {
/*            
 *            ATTENTION CODE CRASH BOUCLE INFINIE !!!!!!!!!
 *            List<Movie> Movies = new List<Movie>();
            var MovieResult = (from s in DB.Movie select s);
            if (!MovieResult.Any())
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Sorry we don't have movie");
                Console.ForegroundColor = ConsoleColor.White;
                return null;
            }
            List<Movie> MoviesRes = MovieResult.ToList();
            for (int i = 0;i < MoviesRes.Count(); i++)
            {
                try { 
                List<DateTime> hours = MoviesRes[i].Hour.ToList();
                Console.WriteLine("{0}:\n   Rea:{1}\n   Produ:{2}\n", MoviesRes[i].Name, MoviesRes[i].Rea, MoviesRes[i].Produc);
                foreach (DateTime hour in hours) { Console.WriteLine("   " + hour + "\n"); }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Sorry we have a probleme : " + e.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            return Movies;*/
        }

        public void NewTicket()
        {

        }
    }
}
