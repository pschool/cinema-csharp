﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cinema
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuDef();
        }
        public static void MenuDef()
        {
            Menu m = new Menu();
            Console.WriteLine("Welcom to Paul Tedesco Cinema");
            int menuChoice = m.MenuDef();

            User UserConnect;
            /* =================Error Check menu=================== */
            while (menuChoice == 0 || menuChoice == -1)
            {
                if (menuChoice == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur: it's not a number");
                    Console.ForegroundColor = ConsoleColor.White;
                    menuChoice = m.MenuDef();
                }
                if (menuChoice == -1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur: it's not a proposed choice");
                    Console.ForegroundColor = ConsoleColor.White;
                    menuChoice = m.MenuDef();
                }

            }
            /*================== Redirection vers Formulaire ==============*/
            string result = "";
            UserConnect = (menuChoice == 1) ? User.Login() : User.Register();
            Console.Clear();
            if (UserConnect.Username.Equals("0") == false && UserConnect.Username.Equals("-1") == false)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine((menuChoice == 1) ? "You are connected" : "You are register");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(UserConnect.ToString());
                Thread.Sleep(2000);
            }
            else
            {
                if (UserConnect.Username.Equals("-1"))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur: One of the fields is not valid");
                    Thread.Sleep(2000);
                    MenuDef();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    throw new Exception("Error : A problem has occurred");
                    Thread.Sleep(2000);
                    MenuDef();
                }
            }

            /*================== Deux menu diff Admin or User ==============*/
            Console.Clear();


            if (UserConnect.Role.Equals("Admin"))
            {
                MenuAdmin();
            }
            else
            {
                MenuUser();
            }
            /* ==========Fin du programme=========== */
            Console.ReadLine();


        }
        public static void MenuAdmin()
        {
            Menu m = new Menu();
            int menuChoice = m.MenuAdmin();
            while (menuChoice == 0 || menuChoice == -1)
            {
                if (menuChoice == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur: it's not a number");
                    Console.ForegroundColor = ConsoleColor.White;
                    menuChoice = m.MenuAdmin();
                }
                if (menuChoice == -1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur: it's not a proposed choice");
                    Console.ForegroundColor = ConsoleColor.White;
                    menuChoice = m.MenuAdmin();
                }
            }
            if (menuChoice == 1)
            {
                menuChoice = m.MenuSalle();
                Salle salle = new Salle();
                switch (menuChoice)
                {
                    case 1:
                        salle.CreateSalle();
                        MenuAdmin();
                        break;
                    case 2:
                        salle.SetMovie();
                        MenuAdmin();
                        break;
                    case 3:
                        MenuAdmin();
                        break;
                }

            }
            else if (menuChoice == 2)
            {
                menuChoice = m.MenuMovie();
                Movie movie = new Movie();
                switch (menuChoice)
                {
                    case 1:
                        movie.CreateMovie();
                        MenuAdmin();
                        break;
                    case 2:
                        Console.Write("Movie Name : ");
                        string name = Console.ReadLine();
                        movie.SetHour(name);
                        MenuAdmin();
                        break;
                    case 3:
                        MenuAdmin();
                        break;
                }
            }
            else
            {
                MenuDef();
            }

        }
        public static void MenuUser()
        {
            Movie movie = new Movie();
            movie.MovieList();
            MenuUser();
        }

    }
}
